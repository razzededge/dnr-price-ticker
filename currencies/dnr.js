let Client = require('node-rest-client').Client;
let datetime = require('node-datetime');


let client = new Client();



client.registerMethod('getValue', 'https://api.coinmarketcap.com/v1/ticker/denarius-dnr/', 'GET');

let getPriceInfo = (logBox) => {
    client.methods.getValue( (data, response )=> {
        let dt = datetime.create();
        let formatted = dt.format('H:M:S d/m/Y');
        logBox.log('Price: {white-fg}'+data[0].price_usd+'{/white-fg}$ on {#aaaaaa-fg}'+formatted+'{/#aaaaaa-fg} price fluctuation  1h: {white-fg}'+data[0].percent_change_1h+'{/white-fg}% 24h: {white-fg}'+data[0].percent_change_24h+'%{/white-fg}');
    });

}


module.exports = { getPriceInfo };


