What it does:
Displays a log of price changes in terminal of DNR Coin from coinmarketcap.com, ticks every minute for now ;)

How to install

Download "Current" Node.js from https://nodejs.org

Launch command line, go to project folder

write: npm install
wait while it downloads needed packets

after it finishes just type:
npm run start


if you like this tool or want me to work on it more, consider donating - any ammount is greatly appreciated, thanks:
DNR: DTU9XK9RymJe2CAvpQnpeFTYgF5nuYELjm
BTC: 13WQHDk8Hj3PFH86BQcAnqag9ovyndHpEE
ETH: 0x3F3481ab92095F428437440C30B2418C32e5810D
BURST: BURST-2DLG-UETU-B7TV-4252W
SIA: 420e9fa53f5fc66f69ba03aec98c88bffecf4aae5c42aaa3079424caa26b755b77f787626e1c



Razzededge 2017