let blessed = require('blessed');
let contrib = require('blessed-contrib');

let DNR = require('./currencies/dnr.js');


let screen = blessed.screen({
    smartCSR: true
});

screen.title = "DNR Price Ticker";


let log = contrib.log({
    fg: "green",
    label: 'DNR Price Log',
    height: "100%",
    width: "80%",
    tags: true,
    border: {type: "line", fg: "cyan"} 
});
    
screen.append(log)

//initial log
DNR.getPriceInfo(log);

setInterval(function() {
   DNR.getPriceInfo(log);
}, 60000);



var spark = contrib.sparkline({
     label: 'Sparklines Coming Soon',
     tags: true,
    border: {type: "line", fg: "cyan"},
    width: '20%',
    height: '20%',
    left: "80%",
    style: { fg: 'blue' }
});

screen.append(spark)

spark.setData(
   [ 'PriceChange', 'Sparkline2'], 
   [ [10, 20, 30, 20, 50, 70, 60, 30, 35, 38]
   , [40, 10, 40, 50, 20, 30, 20, 20, 19, 40]]);




// let updateBox = blessed.box({
//         top: '90%',
//         left: '90%',
//         width: '10%',
//         height: '10%',
//         content: 'Will update in: ',
//         tags: true,
//         border: {
//             type: 'line'
//         },
//         style: {
//             fg: 'white',
//             bg: 'black',
//             border: {
//                 fg: 'black'
//             }
//         }
// });

// screen.append(updateBox);
            



screen.render();





